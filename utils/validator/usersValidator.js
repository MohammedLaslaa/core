const Joi = require("joi");
const { ErrorGenerator } = require("ekonsilio-module");

module.exports = async function (entity) {
	const errGen = new ErrorGenerator();
	const schema = Joi.object({
		lastName: Joi.string().error((err) =>
			errGen.getError(err, "lastName", 404, "Champ_invalide", {
				more: "data info",
			})
		),
		integration: Joi.string(),
		email: Joi.string().email(),
		firstName: Joi.string(),
	});

	return await schema.validateAsync(entity);
};
