"use strict";

const DbService = require("moleculer-db");
const MongooseAdapter = require("moleculer-db-adapter-mongoose");
const Integration = require("../models/Integration");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "integration",
	adapter: new MongooseAdapter(
		"mongodb://v0-ekonsilio:v0-ekonsilio@localhost:27017/v0-test?authSource=admin"
	),
	model: Integration,

	mixins: [DbService],

	/**
	 * Settings
	 */
	settings: {},

	/**
	 * Actions
	 */
	actions: {},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
