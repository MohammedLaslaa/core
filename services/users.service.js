"use strict";
const { Context } = require("moleculer");
const DbMixin = require("../mixins/db.mixin");
const usersValidator = require("../utils/validator/usersValidator");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "users",
	mixins: [DbMixin("users")],

	/**
	 * Settings
	 */
	settings: {
		populates: {
			integration: "integration.get",
		},
		entityValidator: usersValidator,
	},

	/**
	 * Actions
	 */
	actions: {
		get: async function handler(ctx) {
			return await ctx.call("users.find", {
				query: { _id: ctx.params.id },
				populate: "integration",
			});
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
