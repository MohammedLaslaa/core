"use strict";

const DbService = require("moleculer-db");
const MongooseAdapter = require("moleculer-db-adapter-mongoose");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = function (collection) {
	const cacheCleanEventName = `cache.clean.${collection}`;

	const schema = {
		mixins: [DbService],

		events: {
			/**
			 * Subscribe to the cache clean event. If it's triggered
			 * clean the cache entries for this service.
			 *
			 * @param {Context} ctx
			 */
			async [cacheCleanEventName]() {
				if (this.broker.cacher) {
					await this.broker.cacher.clean(`${this.fullName}.*`);
				}
			},
		},

		methods: {
			/**
			 * Send a cache clearing event when an entity changed.
			 *
			 * @param {String} type
			 * @param {any} json
			 * @param {Context} ctx
			 */
			async entityChanged(type, json, ctx) {
				ctx.broadcast(cacheCleanEventName);
			},
		},

		async started() {
			this.logger.info(`The '${collection}' started`);
		},
	};

	if (process.env.NODE_ENV && process.env.NODE_ENV == "dev") {
		process.env.MONGO_URI =
			"mongodb://v0-ekonsilio:v0-ekonsilio@localhost:27017/v0-test?authSource=admin";

		schema.adapter = new MongooseAdapter(process.env.MONGO_URI);
		if (collection === "users") {
			schema.model =
				mongoose.models[collection] || // check if the model is already exist...
				mongoose.model(
					collection,
					new Schema({
						lastName: {
							type: String,
							trim: true,
							required: true,
						},
						firstName: { type: String, trim: true, default: "" },
						email: { type: String, required: true, unique: true },
						integration: {
							type: Schema.ObjectId,
							required: true,
							ref: "integration",
						},
					})
				);
		}

		// else if (collection === "integration") {
		// 	schema.model =
		// 		mongoose.models[collection] ||
		// 		mongoose.model(
		// 			collection,
		// 			new Schema({
		// 				address: { type: String, trim: true, required: true },
		// 				name: { type: String, trim: true, required: true },
		// 			})
		// 		);
		// }
	}

	return schema;
};
