"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const IntegrationSchema = new Schema({
	address: { type: String, trim: true, required: true },
	name: { type: String, trim: true, required: true, unique: true },
});

module.exports = mongoose.model("Integration", IntegrationSchema);
